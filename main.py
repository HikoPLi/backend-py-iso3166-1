import regionCountryCode
import principalSubdivisionsCode


def main():
    regionCountryCode.get_regionCountry_code()
    principalSubdivisionsCode.city_code()


if __name__ == "__main__":
    main()
