# Run
Run  

    python main.py

in terminal.

# For regionCountryCode.py


For country/region code, the format is as follows:
        
    {
        "region/country": "Hong Kong",
        "regionCountryCode": "HK"
    }
    
The newly generated file will be saved as a .json file.
You can view "regionCountryCode.json" in the folder.


# For principalSubdivisions.py --> Not Finish Yet

For city code, the format is as follows:  
        
    {
        "principalSubdivisionName": "Hong Kong",
        "principalSubdivisionsCode": "HK",
        "regionCountryCode": "HK"
    },
    {
        "principalSubdivisionName": "Tai Pei",
        "principalSubdivisionsCode": "TP",
        "regionCountryCode": "TW"
    },
    {
        "principalSubdivisionName": "Guang Dong",
        "principalSubdivisionsCode": "GD",
        "regionCountryCode": "CHN"
    },
    {
        "principalSubdivisionName": "California",
        "principalSubdivisionsCode": "CA",
        "regionCountryCode": "US"
    }

The newly generated file will be saved as a .json file.
You can view "principalSubdivisionsCode.json" in the folder.