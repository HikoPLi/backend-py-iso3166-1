import json

"""
For country/region code, the format is as follows:
        
    {
        "region/country": "Hong Kong",
        "regionCountryCode": "HK"
    }
The newly generated file will be saved as a .json files.
You can view "regionCountryCode.json" in the folder.
"""


def save_regionCountry_code_file(objects):
    with open("regionCountryCode.json", "r") as file:
        data = json.load(file)

    data.append(objects)

    with open("regionCountryCode.json", "w") as file:
        json.dump(data, file, indent=2)


def get_regionCountry_code():
    with open("regionCountryCode.json", "w") as file:
        file.write("[]")

    with open("ISO3166-1.json") as countryCodeFile:
        jsonFile = json.load(countryCodeFile)
        for dictionary in jsonFile:
            for code, name in dictionary.items():
                objects = {"region/country": name, "regionCountryCode": code}
                save_regionCountry_code_file(objects)
