import json

"""
For city code, the format is as follows:  
        
    {
        "principalSubdivisionName": "Hong Kong",
        "principalSubdivisionsCode": "HK",
        "regionCountryCode": "HK"
    },
    {
        "principalSubdivisionName": "Tai Pei",
        "principalSubdivisionsCode": "TP",
        "regionCountryCode": "TW"
    },
    {
        "principalSubdivisionName": "Guang Dong",
        "principalSubdivisionsCode": "GD",
        "regionCountryCode": "CHN"
    },
    {
        "principalSubdivisionName": "California",
        "principalSubdivisionsCode": "CA",
        "regionCountryCode": "US"
    }

The newly generated file will be saved as a json files.
You can view "cityCode.json" in the folder.
"""


def city_code():
    return
